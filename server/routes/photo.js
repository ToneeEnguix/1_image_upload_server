const router = require("express").Router();
const controller = require("../controllers/photo.js");

router.post("/post-photo", controller.post_photo);
router.post("/fetch-photos", controller.fetch_photos);
router.post("/delete-photo", controller.delete_photo);

module.exports = router;
