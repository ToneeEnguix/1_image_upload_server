const mongoose = require("mongoose");

const imagesSchema = new mongoose.Schema({
  pathname: { type: String, required: true },
  filename: { type: String, required: true },
});

module.exports = mongoose.model("images", imagesSchema);
