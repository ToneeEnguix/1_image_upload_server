const app = require("express")();
require("dotenv").config();
const port = process.env.PORT || 4040;
app.use(require("cors")());

(async function () {
  try {
    await require("mongoose").connect(process.env.MONGO, {
      useUnifiedTopology: true,
      useNewUrlParser: true,
    });
    console.log("Connected to the DB ✅");
  } catch (error) {
    console.log("ERROR: Your DB is not running, start it up ☢️");
  }
})();

app.use("/assets", require("express").static(__dirname + "/files"));

app.use("/", require("./routes/photo.js"));

app.listen(port, () => console.log(`🚀🚀🚀 Listening on port: ${port}`));
